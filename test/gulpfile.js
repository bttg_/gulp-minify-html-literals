const {
  src,
  dest
} = require('gulp'),
  minifyHtmlLiterals = require('gulp-minify-html-literals');

const _test = () =>
  src([
    'src*.js'
  ]).pipe(minifyHtmlLiterals()).pipe(dest('dist/'));

module.exports = {
  test: _test
};