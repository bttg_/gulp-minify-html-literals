/* eslint-disable */ 
class BttgTestWidget extends $.classes.Widget {

  constructor() {

    super();

    this.test = 'test';

  }

  template() {
    return html`
      <h1>Test 1</h1>
      <p>This is a ${this.test}.</p>
      <p>This HTML should be minified.</p>
      `;
  }

  render() {
    return this.template();
  }

  static get is() {
    return 'bttg-test-widget';
  }

}

$.classes.Widget.def(BttgTestWidget.is, BttgTestWidget);