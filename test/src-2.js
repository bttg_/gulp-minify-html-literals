/* eslint-disable */ 
class BttgTestWidget extends $.classes.Widget {

  constructor() {

    super();

    this.test = 'test';

  }

  template() {
    return html `
      <h1>Test 2</h1>
      <p>This is another ${this.test}.</p>
      <p>This HTML should be minified too.</p>
      `;
  }

  render() {
    return this.template();
  }

  static get is() {
    return 'bttg-test-widget';
  }

}

$.classes.Widget.def(BttgTestWidget.is, BttgTestWidget);